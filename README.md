![Banner](materiales/cr-banner.png)

<!---
<img alt="" style="border-width:0" src="https://www.dropbox.com/s/b8ls807rceiuqr1/cr-banner.png?raw=1">
--> 

# Física de Astropartículas - Primer semestre de 2019

**Curso destinado a estudiantes de Maestría o Doctorado en Física o Astrofísica. Programa del Doble Doctorado en Astrofísica (DDAp), [ITeDA](http://www.iteda.cnea.gov.ar/)-[UNSAM](http://www.unsam.edu.ar/) y [KIT](https://www.kit.edu/)**

***(C) [2017](https://gitlab.com/asoreyh/astroparticulas/tags), [2018](https://gitlab.com/asoreyh/astroparticulas/tags), [2019](https://gitlab.com/asoreyh/astroparticulas/) - Dr. Hernán Asorey ([@asoreyh](https://twitter.com/asoreyh/))***

### [Programa de la materia](https://gitlab.com/asoreyh/astroparticulas/blob/master/materiales/fisica-de-astroparticulas-syllabus-2019.pdf)

### [Clases](https://gitlab.com/asoreyh/astroparticulas/tree/master/clases)

### [Guías de ejercicios](https://gitlab.com/asoreyh/astroparticulas/tree/master/guias)

### [Materiales](https://gitlab.com/asoreyh/astroparticulas/tree/master/materiales)

## Objetivo

Que el estudiante adquiera una perspectiva general y moderna de la física de astropartículas y algunas de sus posibles aplicaciones.

## Nivel
Destinado a estudiantes de Maestría o Doctorado en Física o Astrofísica. Se recomienda conocer técnicas básicas de programación (cualquier lenguaje) y tener un manejo confortable del sistema operativo Linux.

## Metodología:
El curso tiene una duración total de ciento veinte (120) horas, y está compuesto por treinta (30) módulos de cuatro (4) horas de duración cada uno. Catorce (14) de ellos combinan clases de pizarrón con presentaciones audiovisuales, buscando la activa participación de los estudiantes en un modelo constructivista. Estos módulos se complementan con seis (6) módulos de laboratorios de detección, de simulación y de análisis de datos, y diez (10) módulos de trabajos grupales o individuales.

## Formas de evaluación:
**Regularización**: entrega de trabajos prácticos y monografía final con tema a elección.

**Aprobación**: final integrador ó cumplir las condiciones de promoción (promedio final mayor o igual a 8). 

## Materiales y herramientas

**Máquina virtual LAGUBUNTU**: en el marco del proyecto [LAGO](http://lagoproject.net/) se ha creado una máquina virtual, basada en [lubuntu](http://lubuntu.net/), una implementación liviana de [ubuntu](https://www.ubuntu.com/), y con todos los códigos necesarios para ejecutar los ejercicios planteados, que está disponible para su descarga en [LAGO_VM_2019.ova en google drive]() (próximamente).

Esta máquina virtual emplea tecnologías de virtualización de [virtualbox](https://www.virtualbox.org/), versión 5.1 o superior.

En ubuntu, puede instalar los pre-requisitos necesarios haciendo:

```bash
sudo apt install virtualbox virtualbox-ext-pack virtualbox-guest-additions-iso virtualbox-guest-utils virtualbox-guest-x11
```

Para otros sistemas operativos consultar las instrucciones para [descargar e instalar](https://www.virtualbox.org/wiki/Downloads). 

En caso de usarla, solicitar por correo el usuario y la contraseña.

## Programa

### [Unidad 1: Fenomenología de Astropartículas](clases/)
Introducción a astrofísica relativista. Mecanismos de producción. Posibles fuentes de astropartículas. Propagación de rayos cósmicos en el medio intergaláctico e interestelar y sus consecuencias observacionales. Transporte heliosférico y magnetosférico. *Laboratorio unidad 1*: Propagación en el medio intergaláctico con [CrPropa v3](https://github.com/CRPropa/CRPropa3/wiki/)
#### Bibliografía de la unidad:
* Günter Sigl, “Astroparticle Physics”, Atlantis Press, 1ra Edición, 2017
* Claus Grupen, “Astroparticle Physics”, Springer, Edición 2005
* Peter Grieder, “Cosmic Rays at Earth”, Elsevier, 1ra Edición, 2001
* Bradley Carrol y Dale Ostlie, “An Introduction to Modern Astrophysics”, 2da Edición, 2017
* Péter Mészáros, “The High Energy Universe”, 1ra Edición, 2010

### [Unidad 2: Lluvias Atmosféricas Extendidas (EAS)](clases/)
Introducción a interacción de la radiación con la materia. La atmósfera de la Tierra y Marte. Modelos de desarrollo de una EAS. Principales características de las EAS iniciadas por fotones,protones y núcleos pesados. Universalidad. Partículas secundarias y su distribución longitudinal y transversal. Principales observables de las EAS. *Laboratorio unidad 2*: Simulación de una EAS en [CORSIKA](http://www-ik.fzk.de/corsika/).
#### Bibliografía de la unidad:
* Peter Grieder, “Extensive Air Showers: High Energy Phenomena and Astrophysical Aspects”, Springer, 1ra Edición, 2010.
* Peter Grieder, “Cosmic Rays at Earth”, Elsevier, 1ra Edición, 2001
* Dieter Heck y otros, “CORSIKA: A Monte Carlo Code to Simulate Extensive Air Showers”, Forschungszentrum Karlsruhe Report FZKA 6019, 1998.
* Dieter Heck y Tanguy Pierog, “Extensive Air Shower Simulations with CORSIKA: A User’s Guide”, versión 7.4600 o siguientes, 2017.

### [Unidad 3: Técnicas de detección de Astropartículas](clases/)
Introducción a técnicas de detección de partículas. Técnicas de detección directa: globos y satélites. Principales observatorios de detección directa. Técnicas de detección indirecta: muestreo longitudinal, lateral y técnica de partícula solitaria. Principales detectores de astropartículas: telescopios de fluorescencia; telescopios Cherenkov; detectores Cherenkov en agua; detección por radio; centelladores; cámaras de placas resistivas; otros detectores. Técnicas de reconstrucción. *Laboratorio unidad 3*: Detección de partículas en centelladores plásticos.
#### Bibliografía de la unidad:
* Peter Grieder, “Cosmic Rays at Earth”, Elsevier, 1ra Edición, 2001
* Glenn Knoll, “Radiation Detection and Measurement”, 4ta Edición, 2010
* Lucio Cerrito, “Radiation and Detectors: Introduction to the Physics of Radiation and Detection Devices”, Springer, 1ra Edición, 2017
* Frank Attix, “Introduction to Radiological Physics and Radiationdosimetry”, John Wiley & Sons, Edición 2004
* 

### [Unidad 4 Aplicaciones y análisis de datos](clases/)
Introducción al análisis de datos. Astronomía gamma y destellos de rayos gamma. Astronomía de partículas cargadas. Meteorología y clima del Espacio; Radiación en el entorno cercano a la Tierra. Ionización atmosférica; Muongrafía. *Laboratorio Unidad 4*: Análisis de datos de meteorología del espacio y del decaimiento del muón en un detector Cherenkov en agua.
#### Bibliografía de la unidad:
* Michael Berthold y David Hand, “Intelligent Data Analysis: An Introduction”, Springer, 2da Edición, 2010
* Péter Mészáros, “The High Energy Universe”, 1ra Edición, 2010
* Carolus Schrijver y George Siscoe (Editores), “Heliophysics: Evolving Solar Activity and the Climates of Space and Earth”, Cambridge University Press, 2da Edición (reimpresión), 2012
* David Griffith. “Introduction to Elementary Particles”, 2da Edición, 2008

## Evolución de la cursada

## Cursada: 
Inicio: 28/05/2019. Finalización: 04/07/2019, según el siguiente cronograma:

## Horarios:
* Martes 09:00-13:00: 28/05, 11/06, 18/06, 25/06, 02/07
* Martes 14:00-18:00: 28/05, 11/06, 18/06, 25/06, 02/07
* Jueves 14:00-18:00: 30/05, 13/06, 27/06, 04/07

### Clases:

| **Clase** | **Modalidad** | **Fecha** |  **Hora** | **Contenidos** |
| :---:   | :---:     | :---: | :---: | :---         |
| U01C01 | Presencial | Mar 28/May/19 | 09:00-13:00 | Presentación, introducción, objetivos, metodología, contenidos mínimos, página web. Introducción a la relatividad. Astrofísica relativista. Detección: área, apertura, exposición, espectro. El espectro de RC. Modelo de Hilas. Aceleración y confinamiento en campos electromagnéticos. Un modelo simple para rodillas y tobillo |
| U01C02 | Presencial | Mar 28/May/19 | 14:00-18:00 | Modelos top-down y bottom-up: mecanismos y consecuencias observacionales. Mecanismos de Fermi de 2do y 1er orden. Posibles fuentes. Deflexión magnética. Propagación en medio intergaláctico e interestelar. Efecto GZK. Consecuencias observacionales.|
| U01C03 | Presencial | Jue 30/May/19 | 14:00-18:00 | El efecto GZK y el espectro UHECR. Consecuencias observacionales. Horizonte de partículas. Composición. Fotones y neutrinos UHE. Observables de composición. Evidencias. Correlación con posibles fuentes. Interpretación de observaciones. Agotamiento de las fuentes. Importancia de estudios de composición.|
|        |         |               |             | **Fin unidad 01** |
| U02C01 | Presencial | Mar 11/Jun/19 | 09:00-13:00 | Interacción de partículas cargadas con la materia. Poder de frenado y poder de frenado másico. Formulación de Bethe. C.S.D.A. Energía y tipo de partícula. Efecto del medio. Pérdidas colisionales y radiativas. Electrones y muones. Longitud de interacción electromagnética. Energía crítica. Definición. Otros medios. Bases de datos NIST: estar, pstar, astar. Pico de Bragg |
| U02C02 | Presencial | Mar 11/Jun/19 | 14:00-18:00 | Interacciónd de fotones con la materia. Atenuación lineal y másica. Efectos fotoeléctrico, Compton y creación de pares. Dominancia como función del Z. Coeficiente de atenuación. Interpretación. La atmósfera terrestre: características generales. Modelos atmosféricos: Lyndsey, ModTran, GDAS. Lluvias atmosféricas extendidas: ejemplos y generalidades. Tipo de primario. Secundarios: la cascada electromagnética, la cascada muónica, la cascada hadrónica. Modelo de Heitler. Definición de Nmax y Xmax. |
| U02C03 | Presencial | Jue 13/Jun/19 | 14:00-18:00 | Observables y definiciones. Dependencia con la energía del primario. Lluvias iniciadas por un fotón o electrón. EAS iniciadas por un hadrón: principales características y diferencias con las cascadas iniciadas por un fotón. Modelo de Glasmacher-Matthews. Lluvia iniciada por un protón. Xmax para protones. |
| U02C04 | Presencial | Mar 18/Jun/19 | 09:00-13:00 | Lluvias iniciadas por un hadrón: el modelo de superposición, Xmax, Nmax, Número de muones, diferencias con un hadrón. Producción de secundarios al piso. Distribución longitudinal (Nishimura-Kamata-Griesen, NKG)  y Gaiser-Hilas) y transversal de secundarios. Funciones de distribución lateral (LDF, NKG y sus variaciones). Densidad de partículas al nivel del detector. |
| U02L01 | Presencial | Mar 18/Jun/19 | 14:00-18:00 | Introducción a CORSIKA. Modelos, unidades, tipos de partículas, primarios y secundarios, campo geomagnético |
| U02L02 | Presencial | Mar 25/Jun/19 | 09:00-13:00 | CORSIKA: desarrollo de la simulación. Archivos de salida. Caracterísiticas. Métodos de análisis. Tipos y características de secundarios. Métodos y herramientas de análisis de datos simulados. |
|        |         |               |             | **Fin unidad 02** |
| U03C01 | Presencial | Mar 25/Jun/19 | 14:00-18:00 | El Observatorio Auger. Técnicas de detección en superficie. Detectores Water Cherenkov |
| U03C02 | Presencial | Mar 02/Jul/19 | 12:00-16:00 | Detectores |
| U03C03 | Presencial | Jue 04/Jul/19 | 14:00-18:00 | Detectores |
|        |         |               |             | **Fin unidad 03** |
| U04C01 | Presencial | Mar 16/Jul/19 | 14:00-18:00 | Aplicaciones |
|        |         |               |             | **Fin unidad 04** |
| U04C02 | Presencial | Vie 23/Ago/19 | 09:00-13:00 | Presentaciones a cargo de los estudiantes |
| U04C03 | Presencial | Vie 13/Sep/19 | 12:00       | Fecha límite para entrega de los ejercicios propuestos |
|        |         |               |             | **Fin del curso** |

### Sobre las clases:

Las clases fueron realizados en [LibreOffice Impress](https://es.libreoffice.org/descubre/impress/), la herramienta de presentaciones de [LibreOffice](https://es.libreoffice.org/). Para poder visualizar correctamente las clases, por favor descárguelo siguiendo este enlace: **[Descargue LibreOffice](https://es.libreoffice.org/descarga/libreoffice-estable/)**. En Windows, puede ser necesario descargar también la fuente [Cabin](https://www.fontsquirrel.com/fonts/download/cabin). Para facilitar la difusión, se incluyen también versiones de las clases en formato pdf. Para visualizarlo, podría necesitar descargar [Acrobat Reader](https://get.adobe.com/es/reader).

### Licencia

**Física de Astropartículas - Primer semestre de 2019**, (c) por Hernán Asorey, 2017, 2018, 2019
<br /><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
<br />Este trabajo se distribuye en forma gratuita bajo la licencia <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es">Licencia Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a>. Debería haber recibido una copia de la licencia junto con este trabajo (cc-by-nc-sa-40-legalcode.txt). En su defecto, visite <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es">http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es</a>.
